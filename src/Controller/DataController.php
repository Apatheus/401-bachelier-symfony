<?php

namespace App\Controller;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Logements;
use App\Entity\Etudiants;
use App\Entity\Connexion;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class DataController extends AbstractController
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    #[Route('/data/{commune?}', name: 'data')]
    public function data(ManagerRegistry $doctrine, $commune = null): Response
    {
        $logementsRepository = $doctrine->getRepository(Logements::class);
        $etudiantsRepository = $doctrine->getRepository(Etudiants::class);

        $logements = [];
        $etudiants = [];

        if (!$commune) {
            // Récupère TOUTES les datas si aucune commune n'est spécifiée
            $allLogements = $logementsRepository->findAll();
            foreach ($allLogements as $logement) {
                $commune = $logement->getCommune();
                if (!isset($logements[$commune])) {
                    $logements[$commune] = 0;
                }
                $logements[$commune] += $logement->getNbLogementsEtudiants();
            }

            $allEtudiants = $etudiantsRepository->findAll();
            foreach ($allEtudiants as $etudiant) {
                $geoNom = $etudiant->getGeoNom();
                if (!isset($etudiants[$geoNom])) {
                    $etudiants[$geoNom] = 0;
                }
                $etudiants[$geoNom] += $etudiant->getEffectif();
            }
        } else {
            $communeMAJ = strtoupper($commune);
            $logement = $logementsRepository->findOneBy(['commune' => $communeMAJ]);
            $etudiants = $etudiantsRepository->findBy(['geoNom' => $commune]);

            if (!$logement) {
                return new JsonResponse(['erreur' => 'Aucun logement trouvé'], Response::HTTP_NOT_FOUND);
            }

            $nbEtudiants = 0;
            foreach ($etudiants as $etudiant) {
                $nbEtudiants += $etudiant->getEffectif();
            }

            $data = ['nbLogementsEtudiants' => $logement->getNbLogementsEtudiants(), 'nbEtudiants' => $nbEtudiants];
            return new JsonResponse($data);
        }

        $etudiantsData = [];
        foreach ($etudiants as $geoNom => $nbEtudiants) {
            $etudiantsData[] = [
                'geoNom' => $geoNom,
                'nbEtudiants' => $nbEtudiants,
            ];
        }

        $logementsData = [];
        foreach ($logements as $commune => $nbLogementsEtudiants) {
            $logementsData[] = [
                'commune' => $commune,
                'nbLogementsEtudiants' => $nbLogementsEtudiants,
            ];
        }

        $data = ['etudiants' => $etudiantsData, 'logements' => $logementsData];
        return new JsonResponse($data);
    }


    #[Route("/login", name: "login", methods: "POST")]
    public function login(Request $request, EntityManagerInterface $entityManager): Response
    {
        $data = json_decode($request->getContent(), true);
        $login = $data['username'] ?? null;
        $password = $data['password'] ?? null;



        // Lookup the user by their username
        $user = $entityManager->getRepository(Connexion::class)
            ->findOneBy(['login' => $login]);

        // Check if user was found
        if (!$user) {
            return new Response("User not found for username '{$login}': Identifiants invalides", Response::HTTP_UNAUTHORIZED);
        }

        // Check if the password matches
        if (!password_verify($password, $user->getPassword())) {
            return new Response("Password does not match for username '{$login}': Identifiants invalides", Response::HTTP_UNAUTHORIZED);
        }

        // Generate and return authentication token for the user
        $token = bin2hex(random_bytes(32)); // Generate a random 32-byte string as the token
        return new JsonResponse(['token' => $token], Response::HTTP_OK);
    }

    /**
     * @Route("/etudiants/update", name="update_etudiant", methods={"PUT"})
     */
    public function updateEtudiants(Request $request, ManagerRegistry $doctrine): Response
    {
        $data = json_decode($request->getContent(), true);
        $geoNom = $data['geoNom'] ?? null;
        $nbEtudiants = $data['nbEtudiants'] ?? null;

        if (!$geoNom || $nbEtudiants === null) {
            return new JsonResponse(['error' => 'Invalid data'], Response::HTTP_BAD_REQUEST);
        }

        $etudiantsRepository = $doctrine->getRepository(Etudiants::class);
        $etudiant = $etudiantsRepository->findOneBy(['geoNom' => $geoNom]);

        if (!$etudiant) {
            $this->logger->error('Etudiant record not found');
            return new JsonResponse(['error' => 'Etudiant record not found'], Response::HTTP_NOT_FOUND);
        }

        $etudiant->setEffectif($nbEtudiants);
        $entityManager = $doctrine->getManager();
        $entityManager->persist($etudiant);
        $entityManager->flush();

        // Return the updated entity as JSON
        return new JsonResponse([
            'id' => $etudiant->getId(),
            'geoNom' => $etudiant->getGeoNom(),
            'nbEtudiants' => $etudiant->getEffectif(),
        ]);
    }

    /**
     * @Route("/logements/update", name="update_logement", methods={"PUT"})
     */
    public function updateLogements(Request $request, ManagerRegistry $doctrine): Response
    {
        $data = json_decode($request->getContent(), true);
        $commune = $data['commune'] ?? null;
        $nbLogementsEtudiants = $data['nbLogementsEtudiants'] ?? null;

        if (!$commune || $nbLogementsEtudiants === null) {
            return new JsonResponse(['error' => 'Invalid data'], Response::HTTP_BAD_REQUEST);
        }

        $logementsRepository = $doctrine->getRepository(Logements::class);
        $logement = $logementsRepository->findOneBy(['commune' => $commune]);

        if (!$logement) {
            $this->logger->error('Logement record not found');
            return new JsonResponse(['error' => 'Logement record not found'], Response::HTTP_NOT_FOUND);
        }

        $logement->setNbLogementsEtudiants($nbLogementsEtudiants);
        $entityManager = $doctrine->getManager();
        $entityManager->persist($logement);
        $entityManager->flush();

        return new JsonResponse([
            'id' => $logement->getId(),
            'commune' => $logement->getCommune(),
            'nbLogementsEtudiants' => $logement->getNbLogementsEtudiants(),
        ]);
    }
}
