<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Connexion;

class MainController extends AbstractController
{
    #[Route('', name: 'home')]
    public function index(): Response
    {
        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    #[Route("/hash-password", name: "hash_password")]
    public function hashPassword(EntityManagerInterface $entityManager): Response
    {
        $login = 'admin'; // Replace with the username of the user you want to update
        $plainTextPassword = 'admin'; // Replace with the current plain-text password

        // Lookup the user by their username
        $user = $entityManager->getRepository(Connexion::class)->findOneBy(['login' => $login]);

        if (!$user) {
            return new Response('User not found', Response::HTTP_NOT_FOUND);
        }

        // Hash the password
        $hashedPassword = password_hash($plainTextPassword, PASSWORD_DEFAULT);
        echo "Hashed password: {$hashedPassword}\n";

        // Update the user's password in the database
        $user->setPassword($hashedPassword);
        $entityManager->persist($user);
        $entityManager->flush();

        return new Response('Password updated successfully', Response::HTTP_OK);
    }
}
