<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Logements
 *
 * @ORM\Table(name="Logements")
 * @ORM\Entity
 */
class Logements
{
    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="code_insee", type="integer", nullable=true)
     */
    public $codeInsee;

    /**
     * @var string|null
     *
     * @ORM\Column(name="commune", type="text", nullable=true)
     */
    public $commune;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nb_logements_etudiants", type="integer", nullable=true)
     */
    public $nbLogementsEtudiants;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeInsee(): ?int
    {
        return $this->codeInsee;
    }

    public function setCodeInsee(?int $codeInsee): self
    {
        $this->codeInsee = $codeInsee;

        return $this;
    }

    public function getCommune(): ?string
    {
        return $this->commune;
    }

    public function setCommune(?string $commune): self
    {
        $this->commune = $commune;

        return $this;
    }

    public function getNbLogementsEtudiants(): ?int
    {
        return $this->nbLogementsEtudiants;
    }

    public function setNbLogementsEtudiants(?int $nbLogementsEtudiants): self
    {
        $this->nbLogementsEtudiants = $nbLogementsEtudiants;

        return $this;
    }
}
