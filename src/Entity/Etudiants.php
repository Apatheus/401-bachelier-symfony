<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Etudiants
 *
 * @ORM\Table(name="Etudiants")
 * @ORM\Entity
 */
class Etudiants
{
    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="annee_universitaire", type="text", nullable=true)
     */
    public $anneeUniversitaire;

    /**
     * @var string|null
     *
     * @ORM\Column(name="geo_nom", type="text", nullable=true)
     */
    public $geoNom;

    /**
     * @var int|null
     *
     * @ORM\Column(name="effectif", type="integer", nullable=true)
     */
    public $effectif;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rentree", type="integer", nullable=true)
     */
    public $rentree;

    /**
     * @var int|null
     *
     * @ORM\Column(name="annee", type="integer", nullable=true)
     */
    public $annee;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnneeUniversitaire(): ?string
    {
        return $this->anneeUniversitaire;
    }

    public function setAnneeUniversitaire(?string $anneeUniversitaire): self
    {
        $this->anneeUniversitaire = $anneeUniversitaire;

        return $this;
    }

    public function getGeoNom(): ?string
    {
        return $this->geoNom;
    }

    public function setGeoNom(?string $geoNom): self
    {
        $this->geoNom = $geoNom;

        return $this;
    }

    public function getEffectif(): ?int
    {
        return $this->effectif;
    }

    public function setEffectif(?int $effectif): self
    {
        $this->effectif = $effectif;

        return $this;
    }

    public function getRentree(): ?int
    {
        return $this->rentree;
    }

    public function setRentree(?int $rentree): self
    {
        $this->rentree = $rentree;

        return $this;
    }

    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(?int $annee): self
    {
        $this->annee = $annee;

        return $this;
    }
}
