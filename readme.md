Readme
==

Ce projet Symfony fait partie intégrante de l'atelier SAE 401, qui consiste à développer une application web utilisant une API. 

L'application que je souhaite créer doit servir à faire le lien entre des données contenant des informations sur le nombre de logements étudiants dans les villes des Hauts-de-Seine ainsi que données sur le nombre d'étudiants dans ces mêmes villes, afin de définir quelles villes des Hauts-de-Seines sont les plus susceptibles d'avoir des logements étudiants de libre.

Les objectifs principaux de la facette Symfony du projet sont décrits ci-dessous.

## Mettre en place des routes
Ces routes doivent permettre de récupérer des données relatives aux villes de Hauts-de-Seine. Pour chaque ville donnée, on souhaite obtenir le nombre d'appartements étudiants qui s'y trouvent ainsi que le nombre d'étudiants. Ces informations seront ensuite transmises dans une carte interactive.

## Authentifier un utilisateur
L'objectif est de créer un utilisateur qui puisse manipuler (modifier, ajouter, supprimer) les données de l'API. L'utilisateur doit pouvoir se connecter grâce à un formulaire depuis la page "Connexion".