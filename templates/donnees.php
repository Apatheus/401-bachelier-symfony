<?php

$logements = [];
if (($handle = fopen("logements.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $logements[] = $data;
    }
    fclose($handle);
}

$etudiants = [];
if (($handle = fopen("etudiants.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $etudiants[] = $data;
    }
    fclose($handle);
}
